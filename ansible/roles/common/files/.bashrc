# .bashrc
# Source global definitions
if [ -f /etc/bashrc ]; then
. /etc/bashrc
fi

alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias ll='ls -la'