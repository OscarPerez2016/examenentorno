# Readme.md
### PREPARACIÓN DE ENTORNOS CON VAGRANT Y ANSIBLE

---

**Máquinas en vagrand**

* apiwww - 192.168.35.31
* apidb  - 192.168.35.32
* apiwww_prod - 192.168.35.33
* apidb_prod  - 192.168.35.34

**playbook's en ansible**
* playbook
* playbook_apiwww
* playbook_apidb

**Uso**

* Desplegar máquinas virtuales
```html
vagrant up
o
vagrant up apiwww
o
vagrant up apidb
o
vagrant up apiwww_prod
o
vagrant up apidb_prod
```

* Provionar entornos. En función del entorno que se desee provisionar, ejecutar:

Para todas las máquinas del entorno de desarrollo
```html
ansible-playbook ansible/playbook.yml -i ansible/inventory/hosts -e entorno=entorno-dev
```
Para la máquina apiwww de desarrollo
```html
ansible-playbook ansible/playbook_apiwww.yml -i ansible/inventory/hosts -e entorno=entorno-dev
```
Para la máquina apidb de desarrollo
```html
ansible-playbook ansible/playbook_apidb.yml -i ansible/inventory/hosts -e entorno=entorno-dev
```

Para todas las máquinas del entorno de produccion
```html
ansible-playbook ansible/playbook.yml -i ansible/inventory/hosts -e entorno=entorno-prod
```
Para la máquina apiwww de produccion
```html
ansible-playbook ansible/playbook_apiwww.yml -i ansible/inventory/hosts -e entorno=entorno-prod
```
Para la máquina apidb de produccion
```html
ansible-playbook ansible/playbook_apidb.yml -i ansible/inventory/hosts -e entorno=entorno-prod
```